$(document).ready(function() {
  $(document).on('click', '.pagination a', function(event) {
    event.preventDefault();
    var page = $(this).attr('href').split('page=')[1];
    employee(page);
  });
});
function employee(page) {
  $.ajax({
    type: "GET",
    url: "/transisi/employee" + "?page=" + page,
    success:function(data) {
      $('#employee').html(data);
    }
  });
}