$(document).ready(function() {
  $(document).on('click', '.pagination a', function(event) {
    event.preventDefault();
    var page = $(this).attr('href').split('page=')[1];
    company(page);
  });
});
function company(page) {
  $.ajax({
    type: "GET",
    url: "/transisi/company" + "?page=" + page,
    success:function(data) {
      $('#company').html(data);
    }
  });
}