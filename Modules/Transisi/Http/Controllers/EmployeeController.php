<?php

namespace Modules\Transisi\Http\Controllers;

use Illuminate\Http\Request;
use Modules\Transisi\Entities\Company;
use Modules\Transisi\Entities\Employee;
use Modules\Transisi\Http\Requests\EmployeeRequest;
use Modules\Transisi\Repositories\CompanyRepository;
use Modules\Transisi\Repositories\EmployeeRepository;
use Nwidart\Modules\Routing\Controller;

class EmployeeController extends Controller
{
    public function __construct(EmployeeRepository $employee, CompanyRepository $company)
    {
        $this->employee = $employee;
        $this->company = $company;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $employee = $this->employee->search([
            "search" => request('search')
        ])->paginate(5);
        $company = Company::all();
        // dd($company);
        if ($request->ajax()) {
            return view('transisi::ajax.company-ajax', [
                "title" => "Company",
                "active" => "company",
            ], compact('employee'))->render();
        }
        return view('transisi::dashboard.employee', [
            "title" => "Employee",
            "active" => "employee",
        ], compact('employee'))->render();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('transisi::dashboard/employee-insert', [
            'title' => 'Employee',
            'company' => Company::all()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EmployeeRequest $request)
    {
        $hasil = $this->employee->store($request);
        return redirect('/transisi/employee');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $employee = Employee::find($id);
        return view('transisi::dashboard.employee-edit', [
            "title" => "title",
            "company" => Company::all()
        ], compact('employee'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EmployeeRequest $request, $id)
    {
        $this->employee->update($request, $id);
        return redirect('transisi/employee');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->employee->destroy($id);
        return redirect('/transisi/employee');
    }
}
