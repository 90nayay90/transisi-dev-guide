<?php

namespace Modules\Transisi\Http\Controllers;



use Barryvdh\DomPDF\Facade as PDF;
// use Barryvdh\DomPDF\PDF;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Modules\Transisi\Entities\Company;
use Modules\Transisi\Http\Requests\CompanyRequest;
use Modules\Transisi\Repositories\CompanyRepository;
use Modules\Transisi\Repositories\EmployeeRepository;

class CompanyController extends Controller
{
    public function __construct(
        CompanyRepository $companyRepository, 
        EmployeeRepository $employeeRepository
    ) {
        $this->companyRepository = $companyRepository;
        $this->employeeRepository = $employeeRepository;
    }

    // public function ajaxPagination(Request $request)
    // {
    //     if (request('search')) {
    //         $company = $this->companyRepository->search([
    //             "search" => request('search')
    //         ]);
    //     }else{
    //         $company = $this->companyRepository->all();
    //     }
    //     if ($request->ajax()) {
    //         return view('dashboard.company-ajax', compact('company'));
    //     }
    //     return view('dashboard.company-ajax',compact('company'));
    // }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $company = $this->companyRepository->search([
            "search" => request('search')
        ])->paginate(5);

        if ($request->ajax()) {
            return view('transisi::ajax.company-ajax', [
                "title" => "Company",
                "active" => "company",
            ], compact('company'))->render();
        }
        return view('transisi::dashboard.company', [
            "title" => "Company",
            "active" => "company",
        ], compact('company'))->render();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('transisi::dashboard.company-insert',[
            "title" => "Insert Company",
            "active" => "company"
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CompanyRequest $request)
    {
        // image
        $logoExtension = $request->file('logoUpload')->extension();
        $logoName = date('dmyHis') . '.' . $logoExtension;
        $path = Storage::putFileAs('company', $request->file('logoUpload'), $logoName);

        $hasil = $this->companyRepository->store($request);
        return redirect('/transisi/company');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $employee = Company::find($id)->Employee()->paginate(5);
        return view("transisi::dashboard/employee", [
            "title" => "Employee",
            "active" => "employee",
            "employee" => $employee
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $company = Company::find($id);
        return view('transisi::dashboard.company-edit',[
            "title" => "Edit Company",
            "active" => "company"
        ], compact('company'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CompanyRequest $request, $id)
    {
        $destination = 'storage/company/' . $request->logoOld;
        if (file::exists($destination)) {
            file::delete($destination);
        }
        // image
        $logoExtension = $request->file('logoUpload')->extension();
        $logoName = date('dmyHis') . '.' . $logoExtension;
        $path = Storage::putFileAs('company', $request->file('logoUpload'), $logoName);
        $hasil = $this->companyRepository->update($request, $id);
        
        return redirect("transisi/company");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $destination = 'Storage/app/company/' . Company::find($id)->logo;
        if (Storage::exists('company/' . Company::find($id)->logo)) {
            File::delete($destination);
        }
        $hasil = $this->companyRepository->destroy($id);
        return redirect('transisi/company');
    }

    //create pdf
    public function EmployeePDF($id)
    {
        $employee = Company::find($id)->employee()->get();
        // dd($employee);
        // return view('transisi::pdf/employee-pdf',['employee'=>$employee]);
 
    	$pdf = PDF::loadview('transisi::pdf/employee-pdf',['employee'=>$employee]);
    	return $pdf->download('laporan-pegawai-pdf');
    }
}
