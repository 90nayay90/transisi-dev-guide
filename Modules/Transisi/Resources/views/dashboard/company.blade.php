@extends('transisi::layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ $title }}</div>
                        <div class="card-body">
                            <a class="btn btn-primary mb-3" href="{{ route('company.create') }}" role="button">Add Company</a>
                            <form action="/transisi/company">
                                <div class="input-group mb-1">
                                    <input type="text" class="form-control" placeholder="Search.." name="search">
                                    <button class="btn btn-outline-dark" type="submit">Search</button>
                                </div>
                            </form>
                            <div id="company">
                                @include('transisi::ajax.company-ajax')
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @push('script')
    <script type="text/javascript" src="{{ asset('js/company.js') }}"></script>
    @endpush
@endsection