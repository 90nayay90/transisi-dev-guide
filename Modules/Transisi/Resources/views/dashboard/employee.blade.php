@extends('transisi::layouts.app')

@section('content')
    <h1 align="center" class="mb-5">Employee</h1>
    <a href="{{ route('employee.create') }}">
        <button type="button" class="btn btn-primary mb-2">Add Employee</button>
    </a>
    <form action="/transisi/employee">
        <div class="input-group mb-1">
            <input type="text" class="form-control" placeholder="Search.." name="search">
            <button class="btn btn-outline-dark" type="submit">Search</button>
        </div>
    </form>
    <div id="employee">
        @include('transisi::ajax.employee-ajax')
    </div>
    @push('script')
    <script type="text/javascript" src="{{ asset('js/employee.js') }}"></script>
    @endpush
@endsection