<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Bootstrap CSS -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">

        <title>Hello, world!</title>
    </head>
    <body>
        <table class="table table-striped">
            <tr>
                <th>No</th>
                <th>Name</th>
                <th>Company</th>
                <th>Email</th>
            </tr>
            @foreach ($employee as $item)
                <?php
                    $company = DB::table('companies')->where('id', $item['company_id'])->get();
                ?>
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $item['name'] }}</td>
                    <td>
                        @foreach ($company as $itemCompany)
                            {{ $itemCompany->name }}
                        @endforeach
                    </td>
                    <td>{{ $item['email'] }}</td>
                </tr>
            @endforeach
        {{-- </table>
    </body>
</html> --}}