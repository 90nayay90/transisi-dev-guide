
<div class="list-group" id='listCompany'>
    @foreach ($company as $item)
        <div class="container">
            <div class="row">
                <a href="/transisi/company/{{ $item['id'] }}/edit">
                    <button type="submit" style="margin-right: 10px">Edit</button>
                </a>
                <form action="/transisi/company/{{ $item['id'] }}" method="POST">
                @csrf
                @method('DELETE')
                    <button type="submit">Delete</button>
                </form>
            </div>
        </div>
        <a href="/transisi/company/{{ $item['id'] }}" class="list-group-item list-group-item-action" aria-current="true">
            <?php
                $total_employee = count(DB::table('employees')->where('company_id', $item['id'])->get());
            ?>
            <img src="{{ asset('/storage/company/' . $item->logo) }}" alt="" width="100px" height="100ox">
            <div class="d-flex w-100 justify-content-between">
                <h5 class="mb-1">{{ $item["name"] }} </h5>
                <small>
                    <form action="/transisi/company/employee_pdf/{{ $item['id'] }}">
                        Employee : {{ $total_employee }}
                        <button type="submit" class="btn btn-success">Download PDF</button>
                    </form>
                </small>
            </div>
            <div>
                {{ $item['email'] }}
            </div>
            <small style="font-style:italic;">{{ $item['website'] }}</small>
        </a>
    @endforeach
</div>
{{-- untuk pagination --}}
{{ $company->links() }}