<?php

namespace Modules\Transisi\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class TransisiDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        // $this->call("OthersTableSeeder");
        
        DB::table('companies')->insert([
            'name' => Str::random(10),
            'email' => Str::random(6) . '@gmail.com',
            'logo' => Str::random(5) . '.jpg',
            'website' => Str::random(8) . '.com'

        ]);

        DB::table('employees')->insert([
            'name' => Str::random(10),
            'email' => Str::random(6) . '@gmail.com',
            'company_id' => '1',
            'status' => 1
        ]);


    }
}
