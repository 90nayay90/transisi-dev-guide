<?php

namespace Modules\Transisi\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Company extends Model
{
    // use HasApiTokens, HasFactory, Notifiable;
    use HasFactory;

    protected $fillable = [
        'name',
        'email',
        'logo',
        'website',
    ];
    
    // protected static function newFactory()
    // {
    //     return \Modules\Transisi\Database\factories\CompaniesFactory::new();
    // }

    public function Employee()
    {
        return $this->hasMany(Employee::class);
    }
}
