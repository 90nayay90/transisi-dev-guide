<?php

namespace Modules\Transisi\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Employee extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'email',
        'status',
        'company_id'
    ];
    
    // protected static function newFactory()
    // {
    //     return \Modules\Transisi\Database\factories\EmployeeFactory::new();
    // }

    public function Company()
    {
        return $this->hasOne(Company::class);
    }
}
