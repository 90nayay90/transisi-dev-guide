<?php

// use App\Http\Controllers\HomeController;

use Illuminate\Support\Facades\Route;
use Modules\Transisi\Http\Controllers\DashboardController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('transisi')->group(function() {
    Route::get('/', [DashboardController::class, 'index'])->middleware('auth');
    Route::resource('company', CompanyController::class)->middleware('auth');
    Route::resource('employee', EmployeeController::class)->middleware('auth');
    Route::get('ajax-pagination',array('as'=>'pagination','uses'=>'CompanyController@ajaxPagination'));
    // Route::get('/company/employeepdf/{id}', [CompanyController::class, 'GetEmployeePDF'])->middleware('auth');
    Route::get('/company/employee_pdf/{id}', 'CompanyController@EmployeePDF');
});
