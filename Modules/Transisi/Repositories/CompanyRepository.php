<?php
namespace Modules\Transisi\Repositories;

use Illuminate\Support\Facades\Storage;
use Modules\Transisi\Entities\Company;
use Modules\Transisi\Entities\Employee;
use Modules\Transisi\Http\Requests\CompanyRequest;
use Illuminate\Support\Facades\File;

Class CompanyRepository{
    protected $company;

    public function __construct(Company $company, Employee $employee)
    {
        $this->company = $company;
        $this->employee = $employee;
    }

    public function all()
    {
        return $this->company->get();
    }

    public function find($id)
    {
        return $this->company->find($id);
    }

    public function search(?array $params)
    {
        $query = $this->company->query();
        if (isset($params['search'])) {
            $query = $query->where('name', 'like', '%' . $params['search'] . '%')
                ->orWhere('email', 'like', '%' .  $params['search'] . '%')
                ->orWhere('website', 'like', '%' . $params['search'] . '%');
        }
        return  $query->latest();
    }

    public function update(CompanyRequest $request, $id)
    {
        // image
        $logoExtension = $request->file('logoUpload')->extension();
        $logoName = date('dmyHis') . '.' . $logoExtension;
        
        $company = Company::find($id);
        $company->update([
                        'name' => $request->name,
                        'email' => $request->email,
                        'logo' => $logoName,
                        'website' => $request->website
        ]);
    }
    
    public function store(CompanyRequest $request)
    {
        // image
        $logoExtension = $request->file('logoUpload')->extension();
        $logoName = date('dmyHis') . '.' . $logoExtension;
        $company = new Company;
        $company->name = $request->input('name');
        $company->email = $request->input('email');
        $company->website = $request->input('website');
        $company->name = $request->input('name');
        $company->logo = $logoName;
        $company->save();
    }

    public function destroy($id)
    {
        $employee = $this->employee->where('company_id', '=', $id);
        $company = $this->company->find($id);
        if (isset($employee)) {
            $employee->delete();
        }
        $company->delete();
    }
}

?>