<?php
namespace Modules\Transisi\Repositories;

use Modules\Transisi\Entities\Employee;
use Modules\Transisi\Http\Requests\EmployeeRequest;

Class EmployeeRepository{
    protected $employee;

    public function __construct(Employee $employee)
    {
        $this->employee = $employee;
    }

    public function all()
    {
        return $this->employee->get();
    }

    public function find($id)
    {
        return $this->employee->find($id);
    }

    public function fetch(array $params)
    {
        $query = $this->employee->query();
        if (isset($params['status'])) {
            $query->where('status', $params['status']);
        }
        return $query = $query;
    }

    public function search(?array $params)
    {
        $query = $this->employee->query();
        if (isset($params['search'])) {
            $query = $query->where('name', 'like', '%' . $params['search'] . '%')
                ->orWhere('email', 'like', '%' .  $params['search'] . '%');
        }
        return  $query->latest();
    }

    public function update(EmployeeRequest $request, $id)
    {
        $employee = Employee::find($id);
        $employee->update([
                        'name' => $request->name,
                        'email' => $request->email,
                        'company_id' => strval($request->company),
        ]);
    }
    
    public function store(EmployeeRequest $request)
    {
        $employee = new Employee;
        $employee->name = $request->input('name');
        $employee->email = $request->input('email');
        $employee->company_id = $request->input('company');
        $employee->status = 1;
        $employee->save();
    }

    public function destroy($id)
    {
        $employee = $this->employee->find($id);
        $employee->delete();
    }
}

?>